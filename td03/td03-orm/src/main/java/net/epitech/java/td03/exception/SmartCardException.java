package net.epitech.java.td03.exception;


public class SmartCardException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 771409590200426358L;

	public SmartCardException(Exception e) {
		super(e);
	}

}
