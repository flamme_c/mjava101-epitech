package net.epitech.java.td05;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import net.epitech.java.td05.model.Person;
import net.epitech.java.td05.model.SmartCard;
import net.epitech.java.td05.model.SmartCardType;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("epitech");
		EntityManager em = factory.createEntityManager();

		Person p = new Person();
		p.setAddress("10 chemin du golf, 83400 Hyeres");
		p.setName("Nicolas");

		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.persist(p);

		transaction.commit();
		SmartCardType type = new SmartCardType();
		type.setTypeName("vitale 2.0");
		transaction.begin();

		em.persist(type);

		transaction.commit();
		
		
		List<Person> persons = em.createNamedQuery("Person.findAll")
				.getResultList();
		List<SmartCardType> types = em
				.createNamedQuery("SmartCardType.findAll").getResultList();

		SmartCard card = new SmartCard();
		card.setPerson(persons.get(0));
		card.setSmartCardType(types.get(0));
		transaction.begin();

		em.persist(card);

		transaction.commit();
		em.close();

	}
}
