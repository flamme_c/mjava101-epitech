package net.epitech.java.td05.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Person database table.
 * 
 */
@Entity
@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p")
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String address;

	private String name;

	//bi-directional many-to-one association to SmartCard
	@OneToMany(mappedBy="person")
	private List<SmartCard> smartCards;

	public Person() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SmartCard> getSmartCards() {
		return this.smartCards;
	}

	public void setSmartCards(List<SmartCard> smartCards) {
		this.smartCards = smartCards;
	}

	public SmartCard addSmartCard(SmartCard smartCard) {
		getSmartCards().add(smartCard);
		smartCard.setPerson(this);

		return smartCard;
	}

	public SmartCard removeSmartCard(SmartCard smartCard) {
		getSmartCards().remove(smartCard);
		smartCard.setPerson(null);

		return smartCard;
	}

}