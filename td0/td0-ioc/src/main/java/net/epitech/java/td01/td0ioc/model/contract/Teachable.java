package net.epitech.java.td01.td0ioc.model.contract;

import com.google.inject.ImplementedBy;

import net.epitech.java.td01.td0ioc.model.inj.EnglishCourse;
import net.epitech.java.td01.td0ioc.model.types.CourseType;

@ImplementedBy(EnglishCourse.class)
public interface Teachable {
	public CourseType getCourse();

	public Nameable getTeacher();

	public String getCourseMaterial();

}
