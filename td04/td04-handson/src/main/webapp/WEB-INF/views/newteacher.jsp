<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/bootstrap-theme.min.css" rel="stylesheet"
	type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nouveau Teacher</title>
</head>
<body>
	<form:form method="post" action="teacher" commandName="teacher">
		<div class="form-group">

			<form:label class="form-control" path="name">name</form:label>
			<form:input path="name" />
			<form:errors path="name" cssClass="error" />
			<br />
			<form:label class="form-control" path="email">email</form:label>
			<form:input path="email" />
			<form:errors path="email" cssClass="error" />
			<br />
			<br /> <input type="submit" class="btn btn-default"
				value="Add Teacher" />
		</div>
	</form:form>
</body>
</html>