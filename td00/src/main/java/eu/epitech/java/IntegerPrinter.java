package eu.epitech.java;

import com.google.common.base.Predicate;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Iterables;
import com.google.common.collect.Range;

public class IntegerPrinter implements NumberPrinter<Integer> {

	public void doPrint(Range<Integer> rng, Predicate<Integer> selector) {
		for (Integer grade : Iterables.filter(
				ContiguousSet.create(rng, DiscreteDomain.integers()), selector)) {
			System.out.println(grade);

		}

	}
}
